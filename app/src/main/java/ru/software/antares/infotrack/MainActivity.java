package ru.software.antares.infotrack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.yandex.mapkit.MapKitFactory;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.software.antares.infotrack.data.ApiClient;
import ru.software.antares.infotrack.data.model.YandexResponse;
import ru.software.antares.infotrack.fragments.LoginFragment;
import ru.software.antares.infotrack.fragments.MapFragment;
import ru.software.antares.infotrack.service.LocationService;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MainActivity extends AppCompatActivity {

    private static MainActivity instance = null;

    Intent locationServiceIntent = null;
    BroadcastReceiver receiver;
    double lat;
    double lon;
    boolean locationReceived = false;
    boolean serviceStopped = false;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MapKitFactory.setApiKey(getResources().getString(R.string.maps_api_key));
        MapKitFactory.initialize(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.font_lato_light))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        instance = this;
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (SharedPreferencesHelper.getLoginStage(this) == 2) {
            replaceFragment(new MapFragment());
        } else {
            showLoginFragment();
        }
        if (isLocationPermissionGranted()) {
            startLocationService();
        }
    }

    @Override
    public void onDestroy() {
        instance = null;
        if (isFinishing()) {
            if (locationServiceIntent != null) {
                stopService(locationServiceIntent);
                if (receiver != null) {
                    unregisterReceiver(receiver);
                    receiver = null;
                }
            }
            ApiClient.clear();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!(getSupportFragmentManager().getBackStackEntryCount() == 0 && MapFragment.getInstance() != null && MapFragment.getInstance().hideBottomSheet())) {
            super.onBackPressed();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Consts.REQUEST_CODE_PERMISSION_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationService();
                }
                break;
        }
    }

    public boolean isLocationPermissionGranted() {
        if (ContextCompat.checkSelfPermission(this, Consts.PERMISSION_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Consts.PERMISSION_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            return true;
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Consts.PERMISSION_COARSE_LOCATION))
            {
                new android.support.v7.app.AlertDialog.Builder(this)
                        .setMessage(R.string.read_gps_message)
                        .setCancelable(false)
                        .setPositiveButton(R.string.ok_btn, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Consts.PERMISSION_FINE_LOCATION, Consts.PERMISSION_COARSE_LOCATION},
                                                Consts.REQUEST_CODE_PERMISSION_LOCATION);
                                    }
                                }
                        ).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Consts.PERMISSION_FINE_LOCATION, Consts.PERMISSION_COARSE_LOCATION},
                        Consts.REQUEST_CODE_PERMISSION_LOCATION);
            }
        }
        return false;
    }

    private void startLocationService() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean shouldStop = intent.getBooleanExtra("shouldStop", false);
                if (shouldStop) {
                    stopService(locationServiceIntent);
                    unregisterReceiver(receiver);
                    receiver = null;
                    serviceStopped = true;
                    Intent newIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(newIntent);
                } else {
                    locationReceived = true;
                    lat = intent.getDoubleExtra("Latitude", 0);
                    lon = intent.getDoubleExtra("Longitude", 0);
                    ApiClient.getInstance(instance).setLatitude(lat);
                    ApiClient.getInstance(instance).setLongitude(lon);
                    ApiClient.getInstance(instance).getGeocoderService().getReversedGeocode("json", String.valueOf(lon) + "," + String.valueOf(lat))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(response -> handleResponse(response), t -> handleLoadingError(t));
                }
            }
        };
        IntentFilter filter = new IntentFilter(LocationService.BROADCAST_ACTION);
        registerReceiver(receiver, filter);

        locationServiceIntent = new Intent(this, LocationService.class);
        startService(locationServiceIntent);
    }

    private void handleResponse(YandexResponse response) {
        if (response == null) return;
        if (response.response.objectCollection.geoObjects.size() == 0) return;
        YandexResponse.GeoObjectInternal object = response.response.objectCollection.geoObjects.get(0).geoObject;
        ApiClient.getInstance(this).setCurrentAddress(object.metaDataProperty.metaData.address.addressString);
        if (MapFragment.getInstance() != null) {
            MapFragment.getInstance().updateLocation(ApiClient.getInstance(this).getLatitude(), ApiClient.getInstance(this).getLongitude(), ApiClient.getInstance(this).getCurrentAddress(), false);
        }
    }

    private void handleLoadingError(Throwable t) {
        Timber.e(t);
    }

    public void addFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    public void replaceFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }

    public void showLoginFragment() {
        LoginFragment fragment = new LoginFragment();
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }
}
