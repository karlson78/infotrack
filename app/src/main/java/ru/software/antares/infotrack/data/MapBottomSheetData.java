package ru.software.antares.infotrack.data;

import java.util.ArrayList;

import ru.software.antares.infotrack.data.model.AddressItem;

public class MapBottomSheetData {
    public static int CHANGE_FROM = 0;
    public static int CHANGE_TO = 1;
    public static int ADD_POINT = 2;

    private int mode;
    private int activePage;
    private ArrayList<AddressItem> points;
    private int category;

    public MapBottomSheetData() {
        clear();
    }

    public void clear() {
        mode = CHANGE_FROM;
        activePage = 1;
        points = new ArrayList<>();
        category = -1;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getActivePage() {
        return activePage;
    }

    public void setActivePage(int activePage) {
        this.activePage = activePage;
    }

    public void addPoint(AddressItem point) {
        this.points.add(point);
    }

    public ArrayList<AddressItem> getPoints() {
        return points;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
