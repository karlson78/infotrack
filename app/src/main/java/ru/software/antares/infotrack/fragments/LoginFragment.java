package ru.software.antares.infotrack.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.software.antares.infotrack.Consts;
import ru.software.antares.infotrack.MainActivity;
import ru.software.antares.infotrack.R;
import ru.software.antares.infotrack.SharedPreferencesHelper;
import ru.software.antares.infotrack.data.ApiClient;

public class LoginFragment extends FragmentWithPauseHandler {
    @BindView(R.id.phone_num)
    TextView tvPhoneNum;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.ll_fields)
    LinearLayout llFieldsContainer;
    @BindView(R.id.emptyview)
    View emptyBottomView;
    @BindView(R.id.btn_login)
    Button btnLogin;

    int minKbHeight = 0;
    int currentKbHeight = 0;
    float density = 0f;

    private WeakReference<Context> mContext;
    private PhoneNumberFormattingTextWatcher numberWatcher = null;
    private TextWatcher textWatcher = null;

    private Disposable getSMSDisposable = null;
    private Disposable authDisposable = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = new WeakReference<Context>(getActivity());
        density = getResources().getSystem().getDisplayMetrics().density;
        initViews(SharedPreferencesHelper.getLoginStage(getActivity()));
        initGlobalLayoutListener();
    }

    private void initViews(int stage) {
        switch (stage) {
            case 0: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    numberWatcher = new PhoneNumberFormattingTextWatcher("RU");
                }else {
                    numberWatcher = new PhoneNumberFormattingTextWatcher();
                }
                etPhone.addTextChangedListener(numberWatcher);
                textWatcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (s.toString().length() == 16) {
                            btnLogin.setEnabled(true);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                btnLogin.setBackground(getResources().getDrawable(R.drawable.btn_v21_no_corners));
                            }
                        } else {
                            btnLogin.setEnabled(false);
                        }
                    }
                };
                etPhone.addTextChangedListener(textWatcher);
                etPhone.setText("");
                etPhone.setHint(R.string.phone_prompt);

                btnLogin.setText(getResources().getString(R.string.btn_more));
                btnLogin.setEnabled(false);
                tvPhoneNum.setVisibility(View.GONE);
            }; break;
            case 1: {
                if (numberWatcher != null) {
                    etPhone.removeTextChangedListener(numberWatcher);
                    numberWatcher = null;
                }
                if (textWatcher != null) {
                    etPhone.removeTextChangedListener(textWatcher);
                    textWatcher = null;
                }
                textWatcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (!TextUtils.isEmpty(s.toString())) {
                            btnLogin.setEnabled(true);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                btnLogin.setBackground(getResources().getDrawable(R.drawable.btn_v21_no_corners));
                            }
                        } else {
                            btnLogin.setEnabled(false);
                        }
                    }
                };
                etPhone.addTextChangedListener(textWatcher);
                etPhone.setText("");
                etPhone.setHint(R.string.sms_code_prompt);

                btnLogin.setText(getResources().getString(R.string.btn_done));
                tvPhoneNum.setVisibility(View.VISIBLE);
                tvPhoneNum.setText(SharedPreferencesHelper.getUserPhone(getActivity()));
            }; break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnLogin.setBackground(getResources().getDrawable(R.drawable.btn_v21_disabled_no_corners));
            btnLogin.setStateListAnimator(null);
        } else {
            btnLogin.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_no_corners));
        }
    }

    private void initGlobalLayoutListener() {
        final Window mRootWindow = getActivity().getWindow();
        llFieldsContainer.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            int screenHeight = llFieldsContainer.getRootView().getHeight();
            Rect r = new Rect();
            View view = mRootWindow.getDecorView();
            view.getWindowVisibleDisplayFrame(r);

            int keyBoardHeight = screenHeight - r.bottom;
            if (minKbHeight == 0 && keyBoardHeight <= screenHeight / 8)
                minKbHeight = keyBoardHeight;
            if (keyBoardHeight != currentKbHeight) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) emptyBottomView.getLayoutParams();
                lp.height = keyBoardHeight - minKbHeight;
                emptyBottomView.setLayoutParams(lp);
                currentKbHeight = keyBoardHeight;
            }
        });
    }

    @Override
    public void onDestroy() {
        hideSearchKeyboard();
        super.onDestroy();
    }

    @Override
    public void closeWindow() {
        if (mContext != null && mContext.get() != null) {
            ((MainActivity) mContext.get()).replaceFragment(new MapFragment());
        }
        mContext = null;
    }

    @OnClick(R.id.btn_login)
    void loginClick() {
        hideSearchKeyboard();
        switch (SharedPreferencesHelper.getLoginStage(getActivity())) {
            case 0: {
                getSMSDisposable = ApiClient.getInstance(getActivity()).getApiService().getSMSCode(etPhone.getText().toString().trim().replace("+", "").replace(" ", "").replace("-", ""), Consts.API2_KEY)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            if (response.getError() == 0) {
                                SharedPreferencesHelper.setLoginStage(getActivity(), 1);
                                SharedPreferencesHelper.setUserPhone(getActivity(), etPhone.getText().toString());
                                initViews(1);
                            } else {
                                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }, t -> {});
            }; break;
            case 1: {
                handler.sendMessageDelayed(handler.obtainMessage(MSG_WHAT, MSG_CLOSE_WINDOW, 0), 200);
                authDisposable = ApiClient.getInstance(getActivity()).getApiService().authUser(SharedPreferencesHelper.getUserPhone(getActivity()).trim().replace("+", "").replace(" ", "").replace("-", ""), Integer.valueOf(etPhone.getText().toString()), Consts.API2_KEY)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            if (response.getError() == 0) {
                                SharedPreferencesHelper.setLoginStage(getActivity(), 2);
                                SharedPreferencesHelper.setUserSmsCode(getActivity(), etPhone.getText().toString());
                                SharedPreferencesHelper.setUserId(getActivity(), response.getUserId());
                                initViews(1);
                            } else {
                                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }, t -> {});
            }; break;
        }
    }

    private void hideSearchKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etPhone.getWindowToken(), 0);
    }
}
