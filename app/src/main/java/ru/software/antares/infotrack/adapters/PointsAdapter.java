package ru.software.antares.infotrack.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.software.antares.infotrack.R;
import ru.software.antares.infotrack.data.ApiClient;
import ru.software.antares.infotrack.fragments.MapFragment;

public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.VH> {
    private WeakReference<Context> mContext;

    public PointsAdapter(Context context) {
        mContext = new WeakReference<Context>(context);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new VH(inflater.inflate(R.layout.points_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return ApiClient.getInstance(mContext.get()).getBottomSheetData().getPoints().size();
    }

    public class VH extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_mark)
        ImageView ivMark;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.btn_entrance)
        Button btnEntrance;
        @BindView(R.id.btn_add)
        ImageButton btnAdd;

        private int position;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(int position) {
            if (position == 0) {
                ivMark.setImageResource(R.drawable.red_icon_from);
                btnEntrance.setVisibility(View.VISIBLE);
                btnAdd.setVisibility(View.GONE);
            } else
            if (position == 1) {
                ivMark.setImageResource(R.drawable.blue_icon_to);
                btnEntrance.setVisibility(View.GONE);
                btnAdd.setVisibility(View.VISIBLE);
            } else {
                ivMark.setImageResource(R.drawable.black_list_mark);
                btnEntrance.setVisibility(View.GONE);
                btnAdd.setVisibility(View.GONE);
            }
            tvAddress.setText(ApiClient.getInstance(mContext.get()).getBottomSheetData().getPoints().get(position).getAddress());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnEntrance.setBackgroundResource(R.drawable.btn_v21_medium_radius);
                btnEntrance.setStateListAnimator(null);
            } else {
                btnEntrance.setBackgroundResource(R.drawable.btn_rounded_medium_radius);
            }
            this.position = position;
        }

        @OnClick(R.id.btn_entrance)
        void btnEntranceClick() {

        }

        @OnClick(R.id.btn_add)
        void btnAddClick() {
            if (MapFragment.getInstance() != null) {
                MapFragment.getInstance().addPoint();
            }
        }

        @Override
        public void onClick(View v) {
            if (position == 1) {
                if (MapFragment.getInstance() != null) {
                    MapFragment.getInstance().changeAddrTo();
                }
            }
        }
    }
}
