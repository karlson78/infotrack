package ru.software.antares.infotrack;

import android.content.Context;

public class SharedPreferencesHelper {
    private static final String PREFERENCES = "preferences";

    private static final int MODE = Context.MODE_PRIVATE | Context.MODE_MULTI_PROCESS;

    public static final String USER_PHONE = "user_phone";

    public static final String USER_SMS_CODE = "user_sms_code";

    public static final String LOGIN_STAGE = "login_stage";

    public static final String USER_ID = "user_id";

    public static String getUserPhone(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(USER_PHONE, "");
    }

    public static void setUserPhone(Context context, String value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(USER_PHONE, value).commit();
    }

    public static String getUserSmsCode(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getString(USER_SMS_CODE, "");
    }

    public static void setUserSmsCode(Context context, String value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putString(USER_SMS_CODE, value).commit();
    }

    public static int getLoginStage(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getInt(LOGIN_STAGE, 0);
    }

    public static void setLoginStage(Context context, int value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putInt(LOGIN_STAGE, value).commit();
    }

    public static int getUserId(Context context) {
        return context.getSharedPreferences(PREFERENCES, MODE).getInt(USER_ID, 0);
    }

    public static void setUserId(Context context, int value) {
        context.getSharedPreferences(PREFERENCES, MODE).edit().putInt(USER_ID, value).commit();
    }
}
