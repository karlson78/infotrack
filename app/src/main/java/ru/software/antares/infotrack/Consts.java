package ru.software.antares.infotrack;

public interface Consts {
    String GEOCODER_URL = "https://geocode-maps.yandex.ru/";
    String ROUTING_URL = "https://api.routing.yandex.net/";
    String API_URL = "https://crm.podorojnik.ru/api2/";

    String API2_KEY = "b372c31204eed906d42ef2dae2b654f6";

    long DEBOUNCE_TEXT_CHANGE_DELAY = 800L;

    String PERMISSION_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    String PERMISSION_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";

    int REQUEST_CODE_PERMISSION_LOCATION = 2;
}
