package ru.software.antares.infotrack.fragments;

import android.os.Message;
import android.support.v4.app.Fragment;

import ru.software.antares.infotrack.PauseHandler;

public abstract class FragmentWithPauseHandler extends Fragment {
    final static int MSG_WHAT = ('O' << 16) + ('C' << 8) + 'F';
    final static int MSG_CLOSE_WINDOW = 1;

    protected MsgHandler handler = new MsgHandler();

    @Override
    public void onResume() {
        super.onResume();

        handler.fragment = this;
        handler.resume();
    }

    @Override
    public void onPause() {
        super.onPause();

        handler.pause();
    }

    @Override
    public void onDestroy() {
        handler.fragment = null;
        super.onDestroy();
    }

    public abstract void closeWindow();

    static class MsgHandler extends PauseHandler {
        protected FragmentWithPauseHandler fragment;

        @Override
        protected boolean storeMessage(Message message) {
            return true;
        }

        @Override
        protected void processMessage(Message message) {
            final FragmentWithPauseHandler fragment = this.fragment;
            if (fragment != null) {
                switch (message.what) {
                    case MSG_WHAT: {
                        switch (message.arg1) {
                            case MSG_CLOSE_WINDOW: {
                                fragment.closeWindow();
                            }; break;
                        }
                    }; break;
                }
            }
        }
    }
}
