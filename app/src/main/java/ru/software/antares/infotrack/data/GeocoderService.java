package ru.software.antares.infotrack.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.software.antares.infotrack.data.model.YandexResponse;

public interface GeocoderService {
    @GET("1.x")
    Observable<YandexResponse> getCoords(
            @Query("format") String format,
            @Query("geocode") String address,
            @Query("ll") String ll,
            @Query("spn") String spn,
            @Query("rspn") int rspn
    );

    @GET("1.x")
    Observable<YandexResponse> getReversedGeocode(
            @Query("format") String format,
            @Query("geocode") String address
    );
}
