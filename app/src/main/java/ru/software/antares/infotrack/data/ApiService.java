package ru.software.antares.infotrack.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.software.antares.infotrack.data.model.ApiResponse;

public interface ApiService {
    @GET("user/getsmscode")
    Observable<ApiResponse> getSMSCode(@Query("phone") String phone, @Query("apikey") String apiKey);

    @GET("user/auth")
    Observable<ApiResponse> authUser(@Query("phone") String phone, @Query("code") int code, @Query("apikey") String apiKey);
}
