package ru.software.antares.infotrack.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.software.antares.infotrack.data.model.YandexRoutingResponse;

public interface RoutingService {
    @GET("v1.0.0/route")
    Observable<YandexRoutingResponse> getRoute(
        @Query("apikey") String apiKey,
        @Query("waypoints") String waypoints
    );
}
