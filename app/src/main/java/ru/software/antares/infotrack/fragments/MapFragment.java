package ru.software.antares.infotrack.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.ScreenPoint;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.map.Callback;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObjectCollection;
import com.yandex.mapkit.map.MapObjectDragListener;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.map.PolylineMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.runtime.image.ImageProvider;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.software.antares.infotrack.Consts;
import ru.software.antares.infotrack.R;
import ru.software.antares.infotrack.Utils;
import ru.software.antares.infotrack.adapters.AddressAdapter;
import ru.software.antares.infotrack.adapters.CategoriesAdapter;
import ru.software.antares.infotrack.adapters.PointsAdapter;
import ru.software.antares.infotrack.data.ApiClient;
import ru.software.antares.infotrack.data.MapBottomSheetData;
import ru.software.antares.infotrack.data.model.AddressItem;
import ru.software.antares.infotrack.data.model.YandexResponse;
import ru.software.antares.infotrack.data.model.YandexRoutingResponse;

public class MapFragment extends Fragment {
    @BindView(R.id.container)
    CoordinatorLayout container;
    @BindView(R.id.mapview)
    MapView mapView;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_change_address)
    TextView tvChangeAddress;
    @BindView(R.id.btn_where)
    Button btnWhere;
    @BindView(R.id.bottom_switcher)
    ViewSwitcher wsBottomView;
    @BindView(R.id.ll_call)
    LinearLayout llCall;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.rv_points)
    RecyclerView rvPoints;
    @BindView(R.id.rv_categories)
    RecyclerView rvCategories;
    @BindView(R.id.btn_call)
    Button btnCall;
    @BindView(R.id.ll_address)
    LinearLayout llAddress;
    @BindView(R.id.tv_close)
    TextView tvClose;
    @BindView(R.id.iv_point_icon)
    ImageView ivPointIcon;
    @BindView(R.id.btn_clear)
    ImageButton btnClear;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.list_container)
    FrameLayout flContainer;
    @BindView(R.id.rv_locations)
    RecyclerView rvLocations;
    @BindView(R.id.tv_unresolved)
    TextView tvUnresolved;
    @BindView(R.id.emptyview)
    View emptyBottomView;
    @BindView(R.id.btn_right)
    ImageButton btnRight;
    @BindView(R.id.btn_current_location)
    ImageButton btnCurrentLocation;

    private WeakReference<Context> mContext;
    private BottomSheetBehavior behavior;
    private static MapFragment instance = null;

    private AddressAdapter adapter;
    private PointsAdapter pointsAdapter;
    private CategoriesAdapter categoriesAdapter;

    int minKbHeight = 0;
    int currentKbHeight = 0;
    int screenHeight = 0;
    float density = 0f;

    private boolean addressChanged = false;
    private double lat;
    private double lon;
    private String currentAddress;

    private PolylineMapObject polylineMapObject = null;

    private Disposable searchDisposable = null;
    private Disposable routeDisposable = null;

    public static MapFragment getInstance() {
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        behavior = BottomSheetBehavior.from(wsBottomView);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        adapter = new AddressAdapter();
        rvLocations.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        rvLocations.setAdapter(adapter);
        rvLocations.setVisibility(View.INVISIBLE);
        tvUnresolved.setVisibility(View.VISIBLE);
        btnWhere.setEnabled(false);
        //hideSearchKeyboard();

        pointsAdapter = new PointsAdapter(view.getContext());
        rvPoints.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        rvPoints.setAdapter(pointsAdapter);

        categoriesAdapter = new CategoriesAdapter(view.getContext());
        rvCategories.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvCategories.setAdapter(categoriesAdapter);

        mapView.getMap().move(
                new CameraPosition(new Point(55.751574, 37.573856), 11.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);
        mapView.getMap().setZoomGesturesEnabled(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        instance = this;
        mContext = new WeakReference<Context>(getActivity());
        density = getResources().getSystem().getDisplayMetrics().density;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnWhere.setBackground(getResources().getDrawable(R.drawable.btn_v21_large_radius));
            btnWhere.setStateListAnimator(null);
            btnCall.setBackground(getResources().getDrawable(R.drawable.btn_v21_yellow_no_corners));
            btnCall.setStateListAnimator(null);
        } else {
            btnWhere.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_rounded_large_radius));
            btnCall.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_yellow_no_corners));
        }

        initSearchView();
        initGlobalLayoutListener();
    }

    @Override
    public void onStop() {
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        MapKitFactory.getInstance().onStart();
        mapView.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (searchDisposable != null) {
            searchDisposable.dispose();
            searchDisposable = null;
        }
        if (routeDisposable != null) {
            routeDisposable.dispose();
            routeDisposable = null;
        }
        if (polylineMapObject != null) {
            mapView.getMap().getMapObjects().clear();
            polylineMapObject = null;
        }
        instance = null;
    }

    void initSearchView() {
        searchDisposable = RxTextView.afterTextChangeEvents(etAddress)
                .map(view -> {
                    Editable editable = view.editable();
                    if (editable == null) return "";
                    return editable.toString();
                })
                .doOnNext(text -> {
                    if (text.length() >= 2) {
                        if (mContext.get() != null) {
                            //ApiClient.getInstance(mContext.get()).getGeocoderService().getCoords("json", text, "37.618920,55.756994", "0.552069,0.400552", 0)
                            ApiClient.getInstance(mContext.get()).getGeocoderService().getCoords("json", text, "37.618920,55.756994", "5,5", 1)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(response -> handleLocationsLoaded(response), t -> handleLoadingError(t));
                        }
                    } else {
                    }
                })
                .debounce(Consts.DEBOUNCE_TEXT_CHANGE_DELAY, TimeUnit.MILLISECONDS)
                .subscribe(text -> {
                });
    }

    private void initGlobalLayoutListener() {
        final Window mRootWindow = getActivity().getWindow();
        wsBottomView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            screenHeight = wsBottomView.getRootView().getHeight();
            Rect r = new Rect();
            View view = mRootWindow.getDecorView();
            view.getWindowVisibleDisplayFrame(r);

            int keyBoardHeight = screenHeight - r.bottom;
            if (minKbHeight == 0 && keyBoardHeight <= screenHeight / 8) {
                minKbHeight = keyBoardHeight;
            } else if (keyBoardHeight != currentKbHeight) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) emptyBottomView.getLayoutParams();
                lp.height = keyBoardHeight - minKbHeight;
                emptyBottomView.setLayoutParams(lp);
                emptyBottomView.forceLayout();
                LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) flContainer.getLayoutParams();
                CoordinatorLayout.LayoutParams lp3 = (CoordinatorLayout.LayoutParams) wsBottomView.getLayoutParams();
                if (keyBoardHeight > currentKbHeight && currentKbHeight == minKbHeight) {
                    //Toast.makeText(getActivity(), wsBottomView.getBottom() + " " + keyBoardHeight + " " + minKbHeight + " " + ((tvUnresolved.getVisibility() == View.VISIBLE) ? "visible" : "invisible") + " " + tvUnresolved.getHeight() + " " + emptyBottomView.getHeight(), Toast.LENGTH_SHORT).show();
                    lp2.height = getActivity().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_collapsed_height);
                    lp3.height = getResources().getDimensionPixelSize(R.dimen.bottom_page_2_collapsed_height) + ((wsBottomView.getBottom() + keyBoardHeight <= screenHeight || rvLocations.getVisibility() == View.VISIBLE || minKbHeight == 0) ? 0 : lp.height); // + ((rvLocations.getVisibility() == View.VISIBLE) ? 0 : (keyBoardHeight - currentKbHeight));
                } else {
                    lp2.height = getActivity().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_height);
                    lp3.height = (ApiClient.getInstance(getActivity()).getBottomSheetData().getActivePage() == 0) ? getResources().getDimensionPixelSize(R.dimen.bottom_page_1_height) : getResources().getDimensionPixelSize(R.dimen.bottom_page_2_height);
                }
                flContainer.setLayoutParams(lp2);
                wsBottomView.setLayoutParams(lp3);
                currentKbHeight = keyBoardHeight;
            }
        });
    }

    private void handleLocationsLoaded(YandexResponse response) {
        ArrayList<YandexResponse.GeoObjectInternal> objectsList = new ArrayList<>();
        for (YandexResponse.GeoObject obj : response.response.objectCollection.geoObjects) {
            String kind = obj.geoObject.metaDataProperty.metaData.kind;
            if (kind.toUpperCase().contains("STREET") || kind.toUpperCase().contains("HOUSE") || kind.toUpperCase().contains("METRO") || kind.toUpperCase().contains("DISTRICT")) {
                objectsList.add(obj.geoObject);
            }
        }

        CoordinatorLayout.LayoutParams lp3 = (CoordinatorLayout.LayoutParams) wsBottomView.getLayoutParams();
        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) flContainer.getLayoutParams();
        if (currentKbHeight == minKbHeight) {
            lp2.height = getActivity().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_height);
            lp3.height = (ApiClient.getInstance(getActivity()).getBottomSheetData().getActivePage() == 0) ? getResources().getDimensionPixelSize(R.dimen.bottom_page_1_height) : getResources().getDimensionPixelSize(R.dimen.bottom_page_2_height);
        } else {
            lp2.height = getActivity().getResources().getDimensionPixelSize(R.dimen.bottom_sheet_collapsed_height);
            lp3.height = getResources().getDimensionPixelSize(R.dimen.bottom_page_2_collapsed_height) + ((objectsList.size() > 0 || minKbHeight == 0 || wsBottomView.getBottom() + (currentKbHeight - minKbHeight) <= screenHeight) ? 0 : (currentKbHeight - minKbHeight));
        }

        if (objectsList.size() > 0) {
            tvUnresolved.setVisibility(View.GONE);
            rvLocations.setVisibility(View.VISIBLE);
            emptyBottomView.setVisibility(View.GONE);
            adapter.update(objectsList);
        } else {
            rvLocations.setVisibility(View.GONE);
            tvUnresolved.setVisibility(View.VISIBLE);
            emptyBottomView.setVisibility(View.VISIBLE);
        }
    }

    private void handleLoadingError(Throwable t) {
        rvLocations.setVisibility(View.INVISIBLE);
        tvUnresolved.setVisibility(View.VISIBLE);
    }

    public void updateLocation(double lat, double lon, String address, boolean forceUpdate) {
        boolean fUpdated = false;
        if ((!addressChanged || forceUpdate) && ApiClient.getInstance(getActivity()).getBottomSheetData().getMode() == MapBottomSheetData.CHANGE_FROM) {
            if (forceUpdate) {
                addressChanged = true;
                hideSearchKeyboard();
                new Handler().postDelayed(() -> getActivity().onBackPressed(), 200);
            }
            this.lat = lat;
            this.lon = lon;
            this.currentAddress = address;
            tvAddress.setText(address);
            AddressItem addressItem = new AddressItem(address, lat, lon);
            if (getActivity() != null) {
                if (ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size() == 0) {
                    ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().add(addressItem);
                } else {
                    ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().set(0, addressItem);
                }
                pointsAdapter.notifyDataSetChanged();
                fUpdated = true;
            }
            btnWhere.setEnabled(true);
            mapView.getMap().move(
                    new CameraPosition(new Point(lat, lon), 16.0f, 0.0f, 0.0f),
                    new Animation(Animation.Type.SMOOTH, 0),
                    null);
        } else
        if (forceUpdate && ApiClient.getInstance(getActivity()).getBottomSheetData().getMode() == MapBottomSheetData.CHANGE_TO) {
            AddressItem addressItem = new AddressItem(address, lat, lon);
            if (getActivity() != null) {
                hideSearchKeyboard();
                if (ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size() == 1) {
                    ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().add(addressItem);
                } else {
                    ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().set(1, addressItem);
                }
                pointsAdapter.notifyDataSetChanged();
                fUpdated = true;
                switchToCallView();
                ApiClient.getInstance(getActivity()).getBottomSheetData().setMode(MapBottomSheetData.CHANGE_FROM);
            }
        } else
        if (forceUpdate && ApiClient.getInstance(getActivity()).getBottomSheetData().getMode() == MapBottomSheetData.ADD_POINT) {
            AddressItem addressItem = new AddressItem(address, lat, lon);
            if (getActivity() != null) {
                hideSearchKeyboard();
                ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().add(addressItem);
                pointsAdapter.notifyDataSetChanged();
                fUpdated = true;
                switchToCallView();
                ApiClient.getInstance(getActivity()).getBottomSheetData().setMode(MapBottomSheetData.CHANGE_FROM);
            }
        }

        if (fUpdated && ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size() >= 2) {
            String waypoints = ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(0).getLatitude() + "," + ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(0).getLongitude();
            for (int i=2; i<ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size(); i++) {
                waypoints = waypoints + "|" + ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(i).getLatitude() + "," + ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(i).getLongitude();
            }
            waypoints = waypoints + "|" + ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(1).getLatitude() + "," + ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(1).getLongitude();
            routeDisposable = ApiClient.getInstance(getActivity()).getRoutingService().getRoute(getResources().getString(R.string.routing_api_key), waypoints)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> handleRouteLoaded(response), t -> {});
        }
    }

    private void handleRouteLoaded(YandexRoutingResponse response) {
        mapView.getMap().getMapObjects().clear();
        ArrayList<Point> points = new ArrayList<>();
        double minLat = 1000;
        double minLon = 1000;
        double maxLat = -1000;
        double maxLon = -1000;
        double duration = 0;
        for (YandexRoutingResponse.Leg leg : response.route.legs) {
            if (leg.status.toUpperCase().contains("OK")) {
                for (YandexRoutingResponse.Step step : leg.steps) {
                    duration += (step.duration + step.waitingDuration);
                    for (int i=0; i<step.polyline.points.size(); i++) {
                        if (points.size() == 0 || points.get(points.size() - 1).getLatitude() != step.polyline.points.get(i).get(0) || points.get(points.size() - 1).getLongitude() != step.polyline.points.get(i).get(1)) {
                            points.add(new Point(step.polyline.points.get(i).get(0), step.polyline.points.get(i).get(1)));
                            if (step.polyline.points.get(i).get(0) < minLat) minLat = step.polyline.points.get(i).get(0);
                            if (step.polyline.points.get(i).get(0) > maxLat) maxLat = step.polyline.points.get(i).get(0);
                            if (step.polyline.points.get(i).get(1) < minLon) minLon = step.polyline.points.get(i).get(1);
                            if (step.polyline.points.get(i).get(1) > maxLon) maxLon = step.polyline.points.get(i).get(1);
                        }
                    }
                }
            }
        }

        tvTime.setText("~ " + Utils.getTimeString(Math.round(duration)) + " " + getResources().getString(R.string.str_time));

        polylineMapObject = mapView.getMap().getMapObjects().addPolyline(new Polyline(points));
        polylineMapObject.setStrokeColor(getResources().getColor(R.color.colorDarkGray));
        polylineMapObject.setStrokeWidth(3.f);
        float zoom = mapView.getMap().getCameraPosition().getZoom();
        mapView.getMap().move(
                new CameraPosition(new Point((minLat + maxLat)/2, (minLon + maxLon)/2), zoom, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);
        ScreenPoint ptBottomLeft = mapView.worldToScreen(new Point(minLat, minLon));
        ScreenPoint ptTopRight = mapView.worldToScreen(new Point(maxLat, maxLon));
        float diffX = ptTopRight.getX() - ptBottomLeft.getX();
        float diffY = ptBottomLeft.getY() - ptTopRight.getY();
        if (diffX > mapView.getWidth() || diffY > mapView.getHeight()) {
            while (diffX > mapView.getWidth() || diffY > mapView.getHeight()) {
                diffX /= 2;
                diffY /= 2;
                zoom -= 1;
            }
        } else {
            while (diffX <= mapView.getWidth() && diffY <= mapView.getHeight()) {
                diffX *= 2;
                diffY *= 2;
                zoom += 1;
            }
            zoom -= 1;
        }
        if (zoom > mapView.getMap().getMaxZoom()) {
            zoom = mapView.getMap().getMaxZoom();
        } else
        if (zoom < mapView.getMap().getMinZoom()) {
            zoom = mapView.getMap().getMinZoom();
        }
        mapView.getMap().move(
                new CameraPosition(new Point((minLat + maxLat)/2, (minLon + maxLon)/2), zoom, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);

        for (int i=0; i<ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size(); i++) {
            PlacemarkMapObject obj = mapView.getMap().getMapObjects().addPlacemark(new Point(ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(i).getLatitude(), ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().get(i).getLongitude()));
            if (i == 0) {
                obj.setIcon(ImageProvider.fromResource(getActivity(), R.drawable.blue_list_mark));
            } else
            if (i == 1) {
                obj.setIcon(ImageProvider.fromResource(getActivity(), R.drawable.red_list_mark));
            } else {
                obj.setIcon(ImageProvider.fromResource(getActivity(), R.drawable.black_list_mark));
            }
        }
    }

    @OnClick(R.id.tv_change_address)
    void changeAddressClick() {
        clearAddressPicker();
        ivPointIcon.setImageResource(R.drawable.red_icon_from);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setMode(MapBottomSheetData.CHANGE_FROM);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setActivePage(1);
    }

    @OnClick(R.id.btn_where)
    void whereClick() {
        if (ApiClient.getInstance(getActivity()).getBottomSheetData().getPoints().size() < 2) {
            changeAddrTo();
        } else {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            switchToCallView();
        }
    }

    public void changeAddrTo() {
        clearAddressPicker();
        ivPointIcon.setImageResource(R.drawable.blue_icon_to);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setMode(MapBottomSheetData.CHANGE_TO);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setActivePage(1);
    }

    public void addPoint() {
        clearAddressPicker();
        ivPointIcon.setImageResource(R.drawable.black_list_mark);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setMode(MapBottomSheetData.ADD_POINT);
        ApiClient.getInstance(getActivity()).getBottomSheetData().setActivePage(1);
    }

    public void clearAddressPicker() {
        llAddress.setVisibility(View.VISIBLE);
        if (ApiClient.getInstance(getActivity()).getBottomSheetData().getActivePage() != 1) {
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) wsBottomView.getLayoutParams();
            lp.height = getResources().getDimensionPixelSize(R.dimen.bottom_page_2_height);
            wsBottomView.setAnimation(null);
            final String tag = (wsBottomView.getCurrentView() != null) ? (String) wsBottomView.getCurrentView().getTag() : "null";
            new Handler().postDelayed(() -> {
                int cnt=(tag.contains("call")) ? 1 : 2;
                for (int i=0; i<cnt; i++) {
                    wsBottomView.showNext();
                }
            }, 150);
            ApiClient.getInstance(getActivity()).getBottomSheetData().setActivePage(1);
        }

        etAddress.setVisibility(View.VISIBLE);
        etAddress.setText("");
        divider.setVisibility(View.VISIBLE);
        rvLocations.setVisibility(View.INVISIBLE);
        tvUnresolved.setVisibility(View.VISIBLE);
        emptyBottomView.setVisibility(View.VISIBLE);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void switchToCallView() {
        int previousPage = ApiClient.getInstance(getActivity()).getBottomSheetData().getActivePage();
        ApiClient.getInstance(getActivity()).getBottomSheetData().setActivePage(0);
        pointsAdapter.notifyDataSetChanged();
        if (previousPage != 0) {
            final String tag = (wsBottomView.getCurrentView() != null) ? (String) wsBottomView.getCurrentView().getTag() : "null";
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) wsBottomView.getLayoutParams();
            lp.height = getResources().getDimensionPixelSize(R.dimen.bottom_page_1_height);
            wsBottomView.invalidate();
            wsBottomView.setInAnimation(getActivity(), R.anim.animation_x_in_enter);
            wsBottomView.setOutAnimation(getActivity(), R.anim.animation_x_in_exit);
            new Handler().postDelayed(() -> {
                int cnt=(tag.contains("call")) ? 2 : 1;
                for (int i=0; i<cnt; i++) {
                    wsBottomView.showPrevious();
                }
            }, 150);
        }
    }

    public boolean hideBottomSheet() {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            etAddress.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            emptyBottomView.setVisibility(View.VISIBLE);
            return true;
        } else {
            return false;
        }
    }

    public void setStreet(String name) {
        etAddress.setText(name);
    }

    @OnClick(R.id.btn_current_location)
    void currentLocationClick() {
        updateLocation(ApiClient.getInstance(getActivity()).getLatitude(), ApiClient.getInstance(getActivity()).getLongitude(), ApiClient.getInstance(getActivity()).getCurrentAddress(), true);
    }

    @OnClick(R.id.btn_clear)
    void clearBtnClick() {
        clearAddressPicker();
    }

    @OnClick(R.id.tv_close)
    void closeClick() {
        if (getActivity() == null) return;
        hideSearchKeyboard();
        if (ApiClient.getInstance(getActivity()).getBottomSheetData().getMode() == MapBottomSheetData.ADD_POINT) {
            switchToCallView();
        } else {
            hideBottomSheet();
        }
    }

    private void hideSearchKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etAddress.getWindowToken(), 0);
    }
}
