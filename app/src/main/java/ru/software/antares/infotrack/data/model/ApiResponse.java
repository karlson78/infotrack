package ru.software.antares.infotrack.data.model;

import com.google.gson.annotations.SerializedName;

public class ApiResponse
{
    @SerializedName("error")
    private int error;
    @SerializedName("message")
    private String message;
    @SerializedName("user_id")
    private int userId;

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }

    public int getUserId() {
        return userId;
    }
}
