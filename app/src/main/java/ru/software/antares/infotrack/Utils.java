package ru.software.antares.infotrack;

public class Utils {
    public static String getTimeString(long time) {
        String res = "";
        int index;
        int cases[] = {2, 0, 1, 1, 1, 2};
        String hours[] = {"час", "часа", "часов"};
        String minutes[] = {"минута", "минуты", "минут"};
        String seconds[] = {"секунда", "секунды", "секунд"};

        if (time < 60) {
            index = (time % 100 > 4 && time % 100 < 20) ? 2 : cases[(int) Math.min(time % 10, 5)];
            res = time + " " + seconds[index];
        } else
        if (time < 3600) {
            long min = time / 60;
            long sec = time % 60;
            index = (min % 100 > 4 && min % 100 < 20) ? 2 : cases[(int) Math.min(min % 10, 5)];
            res = min + " " + minutes[index];
            if (min > 0) {
                index = (sec % 100 > 4 && sec % 100 < 20) ? 2 : cases[(int) Math.min(sec % 10, 5)];
                res = res + " " + min + " " + seconds[index];
            }
        } else {
            long min = (time + 29) / 60;
            long hour = min / 60;
            min = min % 60;
            index = (hour % 100 > 4 && hour % 100 < 20) ? 2 : cases[(int) Math.min(hour % 10, 5)];
            res = hour + " " + hours[index];
            if (min > 0) {
                index = (min % 100 > 4 && min % 100 < 20) ? 2 : cases[(int) Math.min(min % 10, 5)];
                res = res + " " + min + " " + minutes[index];
            }
        }

        return res;
    }
}
