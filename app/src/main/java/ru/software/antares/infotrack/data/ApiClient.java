package ru.software.antares.infotrack.data;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ru.software.antares.infotrack.Consts;

public class ApiClient {
    private static ApiClient instance = null;
    private Gson gson;
    private WeakReference<Context> mContext;

    private final GeocoderService geocoderService;
    private final RoutingService routingService;
    private final ApiService apiService;
    private double lat;
    private double lon;
    private String currentAddress;
    private MapBottomSheetData bottomSheetData;

    public static ApiClient getInstance(Context context) {
        if (instance == null) {
            instance = new ApiClient(context);
        }
        return instance;
    }

    public static void clear() {
        if (instance.bottomSheetData != null) {
            instance.bottomSheetData.clear();
        }
        instance = null;
    }

    public Gson getGson() {
        return gson;
    }

    public GeocoderService getGeocoderService() {
        return geocoderService;
    }

    public RoutingService getRoutingService() {
        return routingService;
    }

    public ApiService getApiService() {
        return apiService;
    }

    public double getLatitude() {
        return lat;
    }

    public void setLatitude(double lat) {
        this.lat = lat;
    }

    public double getLongitude() {
        return lon;
    }

    public void setLongitude(double lon) {
        this.lon = lon;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public MapBottomSheetData getBottomSheetData() {
        return bottomSheetData;
    }

    public ApiClient(Context context) {
        HttpLoggingInterceptor httpLoggingInterceptor =
                new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .pingInterval(10, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .build();
        gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        Retrofit retrofitGeocode = new Retrofit.Builder()
                .baseUrl(Consts.GEOCODER_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        geocoderService = retrofitGeocode.create(GeocoderService.class);
        Retrofit retrofitRouting = new Retrofit.Builder()
                .baseUrl(Consts.ROUTING_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        routingService = retrofitRouting.create(RoutingService.class);
        Retrofit retrofitApi = new Retrofit.Builder()
                .baseUrl(Consts.API_URL)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        apiService = retrofitApi.create(ApiService.class);

        bottomSheetData = new MapBottomSheetData();
    }

    public class DateDeserializer implements JsonDeserializer<Date> {

        @Override
        public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            String date = element.getAsString();

            if (date.toUpperCase().contains("NULL")) {
                return new Date();
            } else {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                try {
                    Date rawDate = formatter.parse(date);
                    Date res = new Date();
                    res.setTime(rawDate.getTime());
                    return res;
                } catch (ParseException e) {
                    return null;
                }
            }
        }
    }
}
