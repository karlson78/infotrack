package ru.software.antares.infotrack.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.software.antares.infotrack.R;
import ru.software.antares.infotrack.data.ApiClient;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.VH> {
    private WeakReference<Context> mContext;

    public CategoriesAdapter(Context context) {
        mContext = new WeakReference<Context>(context);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            return new VH(inflater.inflate(R.layout.active_category_item, parent, false));
        } else {
            return new VH(inflater.inflate(R.layout.inactive_category_item, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (ApiClient.getInstance(mContext.get()).getBottomSheetData().getCategory() == position) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int getItemCount() {
        return 3; //TODO: get categories list from server
    }

    public class VH extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.iv_category)
        ImageView ivCategory;
        @BindView(R.id.iv_info)
        ImageView ivInfo;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_price)
        TextView tvPrice;

        private int position;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(int position) {
            switch (position) {
                case 0: {
                    ivCategory.setImageResource(R.drawable.car1);
                    tvName.setText(R.string.category_econom);
                    tvPrice.setText("988 руб.");
                }; break;
                case 1: {
                    ivCategory.setImageResource(R.drawable.car2);
                    tvName.setText(R.string.category_comfort);
                    tvPrice.setText("1346 руб.");
                }; break;
                case 2: {
                    ivCategory.setImageResource(R.drawable.car3);
                    tvName.setText(R.string.category_comfort_plus);
                    tvPrice.setText("1532 руб.");
                }; break;
            }
            if (ApiClient.getInstance(mContext.get()).getBottomSheetData().getCategory() == position) {
                ivInfo.setVisibility(View.VISIBLE);
            } else {
                ivInfo.setVisibility(View.GONE);
            }
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            ApiClient.getInstance(mContext.get()).getBottomSheetData().setCategory(position);
            notifyDataSetChanged();
        }
    }
}
