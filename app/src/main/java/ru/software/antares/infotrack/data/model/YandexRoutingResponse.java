package ru.software.antares.infotrack.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YandexRoutingResponse {
    @SerializedName("traffic")
    public String trafficInfoType;
    @SerializedName("route")
    public Route route;

    public static class Route {
        @SerializedName("legs")
        public List<Leg> legs;
    }

    public static class Leg {
        @SerializedName("status")
        public String status;
        @SerializedName("steps")
        public List<Step> steps;
    }

    public static class Step {
        @SerializedName("length")
        public double length;
        @SerializedName("duration")
        public double duration;
        @SerializedName("waiting_duration")
        public double waitingDuration;
        @SerializedName("mode")
        public String mode;
        @SerializedName("polyline")
        public Polyline polyline;
    }

    public static class Polyline {
        @SerializedName("points")
        public List<List<Double>> points;
    }
}
