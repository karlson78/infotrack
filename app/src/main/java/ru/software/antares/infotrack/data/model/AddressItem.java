package ru.software.antares.infotrack.data.model;

public class AddressItem {
    private String address;
    private double lat;
    private double lon;

    public AddressItem() {

    }

    public AddressItem(String address, double lat, double lon) {
        this.address = address;
        this.lat = lat;
        this.lon = lon;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return lat;
    }

    public double getLongitude() {
        return lon;
    }
}
