package ru.software.antares.infotrack.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.software.antares.infotrack.R;
import ru.software.antares.infotrack.data.model.YandexResponse;
import ru.software.antares.infotrack.fragments.MapFragment;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.VH> {
    private ArrayList<YandexResponse.GeoObjectInternal> objects = new ArrayList<>();

    @Override
    public AddressAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new VH(inflater.inflate(R.layout.address_item, parent, false));
    }

    @Override
    public void onBindViewHolder(AddressAdapter.VH holder, int position) {
        holder.bind(objects.get(position), position);
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    public void update(List<YandexResponse.GeoObjectInternal> objectsList) {
        this.objects.clear();
        this.objects.addAll(objectsList);
        notifyDataSetChanged();
    }

    public class VH extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.item_descr)
        TextView tvDescr;
        @BindView(R.id.item_name)
        TextView tvName;

        private int position;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(YandexResponse.GeoObjectInternal object, int position) {
            tvDescr.setText(object.description);
            tvName.setText(object.name);
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            if (objects.get(position).metaDataProperty.metaData.kind.toUpperCase().contains("STREET") || objects.get(position).metaDataProperty.metaData.kind.toUpperCase().contains("DISTRICT")) {
                if (MapFragment.getInstance() != null) {
                    MapFragment.getInstance().setStreet(objects.get(position).name);
                }
            } else {
                String coords = objects.get(position).point.pos;
                int pos = coords.indexOf(" ");
                double lat = Double.valueOf(coords.substring(pos+1, coords.length()));
                double lng = Double.valueOf(coords.substring(0, pos));
                if (MapFragment.getInstance() != null) {
                    MapFragment.getInstance().updateLocation(lat, lng, objects.get(position).name, true);
                }
            }
        }
    }
}
