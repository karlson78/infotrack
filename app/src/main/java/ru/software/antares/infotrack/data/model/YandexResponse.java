package ru.software.antares.infotrack.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YandexResponse {
    @SerializedName("response")
    public YandexResponseInternal response;

    public static class YandexResponseInternal {
        @SerializedName("GeoObjectCollection")
        public GeoObjectCollection objectCollection;
    }

    public static class GeoObjectCollection {
        @SerializedName("featureMember")
        public List<GeoObject> geoObjects;
    }

    public static class GeoObject {
        @SerializedName("GeoObject")
        public GeoObjectInternal geoObject;
    }

    public static class GeoPoint {
        @SerializedName("pos")
        public String pos;
    }

    public static class GeoObjectInternal {
        @SerializedName("metaDataProperty")
        public MetaDataProperty metaDataProperty;
        @SerializedName("Point")
        public GeoPoint point;
        @SerializedName("description")
        public String description;
        @SerializedName("name")
        public String name;
    }

    public static class MetaDataProperty {
        @SerializedName("GeocoderMetaData")
        public GeoObjectMetaData metaData;
    }

    public static class GeoObjectMetaData {
        @SerializedName("kind")
        public String kind;
        @SerializedName("text")
        public String text;
        @SerializedName("precision")
        public String precision;
        @SerializedName("Address")
        public GeoObjectAddress address;
    }

    public static class GeoObjectAddress {
        @SerializedName("country_code")
        public String countryCode;
        @SerializedName("postal_code")
        public String postalCode;
        @SerializedName("formatted")
        public String addressString;
    }
}
